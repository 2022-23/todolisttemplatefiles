package es.cipfpbatoi.controllers;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.entidades.Tarea;
import es.cipfpbatoi.modelo.repositorios.TareaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TareaController {

    @Autowired
    private TareaRepository tareaRepository;

    @GetMapping(value = "/tarea-del")
    @ResponseBody
    public String deleteAction(@RequestParam int codTarea) throws NotFoundException {
        Tarea tarea = tareaRepository.get(codTarea);
        tareaRepository.remove(tarea);
        return "Tarea Borrada " + codTarea + "con éxito";
    }

    @GetMapping("/tarea")
    public String getTareaView(@RequestParam int codTarea, Model modelAndView) throws NotFoundException {
        Tarea tarea = tareaRepository.get(codTarea);
        modelAndView.addAttribute("tarea", tarea);
        modelAndView.addAttribute("usuarioLogueado", "Alex");
        return "tarea_details_view";
    }

    @GetMapping("/tarea-add")
    public String tareaFormActionView(){
        return "tarea_form_view";
    }

    @PostMapping(value = "/tarea-add")
    public String postAddAction(@RequestParam Map<String, String> params) {
        int code = Integer.parseInt(params.get("code"));
        String user = params.get("user");
        String descripcion = params.get("description");
        String prioridad = params.get("priority");
        String realizadaString = params.get("realizada");
        boolean realizada = realizadaString != null && realizadaString.equalsIgnoreCase("true");
        Tarea.Priority priority = Tarea.Priority.fromText(prioridad);
        String horaExpiracionString = params.get("expiration_time");
        String fechaExpiracionString = params.get("expiration_date");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime fechaHora = LocalDateTime.parse(fechaExpiracionString + " " + horaExpiracionString, timeFormatter);
        Tarea tarea = new Tarea(code, user, descripcion, priority, fechaHora, realizada);
        tareaRepository.add(tarea);
        return "redirect:/tareas";
    }

    @GetMapping(value = "/tareas")
    public String tareaList(Model model, @RequestParam HashMap<String, String> params) {
        String usuario = params.get("usuario");
        ArrayList<Tarea> tareas;
        if (usuario != null && !usuario.isEmpty()) {
            tareas = tareaRepository.findAll(usuario);
            model.addAttribute("usuario", usuario);
        } else {
            tareas = tareaRepository.findAll();
        }
        model.addAttribute("tareas", tareas);
        return "list_tarea_view";
    }

}
