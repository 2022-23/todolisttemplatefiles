package es.cipfpbatoi.modelo.entidades;

import java.time.LocalDateTime;
import java.util.InputMismatchException;
import java.util.Objects;

public class Tarea {

    public enum Priority {

        ALTA, BAJA, MEDIA;

        public static Priority fromText(String value){
            if (value.equals("alta")) {
                return Priority.ALTA;
            } else  if (value.equals("baja")) {
                return Priority.BAJA;
            } else  if (value.equals("media")) {
                return Priority.MEDIA;
            }
            throw new InputMismatchException("El valor " + value + " es inválido");
        }
    }

    private int codigo;

    private String usuario;

    private String descripcion;

    private LocalDateTime creadoEn;

    private Priority prioridad;

    private LocalDateTime vencimiento;

    private boolean realizado;

    public Tarea(int codigo) {
       this.codigo = codigo;
    }
    public Tarea(int codigo, String usuario, String descripcion) {
         this(codigo, usuario, descripcion, Priority.MEDIA, LocalDateTime.now().plusDays(2), false);
    }

    public Tarea(int codigo, String usuario, String descripcion, LocalDateTime vencimiento) {
        this(codigo, usuario, descripcion, Priority.MEDIA, vencimiento, false);
    }

    public Tarea(int codigo, String usuario, String descripcion, Priority priority, LocalDateTime vencimiento, boolean realizado) {
        this(codigo, usuario, descripcion, LocalDateTime.now(), priority, vencimiento, realizado);
    }

    public Tarea(int codigo, String usuario, String descripcion, LocalDateTime creadoEn, Priority priority, LocalDateTime vencimiento, boolean realizado) {
        this.codigo = codigo;
        this.usuario = usuario;
        this.descripcion = descripcion;
        this.prioridad = priority;
        this.creadoEn = creadoEn;
        this.vencimiento = vencimiento;
        this.realizado = realizado;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public LocalDateTime getCreadoEn() {
        return creadoEn;
    }

    public Priority getPrioridad() {
        return prioridad;
    }

    public LocalDateTime getVencimiento() {
        return vencimiento;
    }

    public int getCodigo() {
        return codigo;
    }

    public boolean isRealizado() {
        return realizado;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tarea)) {
            return false;
        }
        Tarea tarea = (Tarea) o;
        return getCodigo() == tarea.getCodigo();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo());
    }
}
