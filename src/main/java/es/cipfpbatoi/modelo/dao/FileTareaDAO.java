package es.cipfpbatoi.modelo.dao;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.entidades.Tarea;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@Service
public class FileTareaDAO implements TareaDaoInterface {

    private static final String DATABASE_FILE = "/databases/tareas.txt";

    private final File file;

    public FileTareaDAO() {
        this.file = new File(getClass().getResource(DATABASE_FILE).getFile());
    }

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id o lanza una excepción si no encuentra
     * una tarea con @id
     *
     * @param id
     * @return
     */
    @Override
    public Tarea getById(int id) throws NotFoundException {
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id
     *
     * @param id
     * @return
     */
    @Override
    public Tarea findById(int id) {
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Devuelve todas las tareas
     * @return
     */
    @Override
    public ArrayList<Tarea> findAll() {
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Devuelve las tareas cuyo atributo realizado coincide con @isRealizada y la fecha de vencimiento coincide @vencimiento
     * Si alguno de los atributos es null no deberá tenerse en cuenta
     *
     * @param isRealizada
     * @param vencimiento
     * @param usuario
     *
     * @return
     */
    @Override
    public ArrayList<Tarea> findAllWithParams(Boolean isRealizada, LocalDate vencimiento, String usuario) {
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Devuelve todas las tareas cuyo usuario coincide con @usuario
     *
     * @param usuario
     * @return
     */
    @Override
    public ArrayList<Tarea> findAllWithParams(String usuario) {
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Añade una tarea al final del archivo siguiendo el formato
     *
     *  5;Batoi;Jugar al badminton;2022-01-06 15:00:00;BAJA;2023-04-06 15:00:00:true
     *
     * @param tarea
     * @return boolean
     */
    @Override
    public void add(Tarea tarea) {
        throw new RuntimeException("Not yet implemented");
    }

    /**
     *  Borra la tarea cuyo atributo codigo coincide con @codigo. El algoritmo más sencillo es:
     *
     *  1. Leer todo el fichero y almacenar el contenido en un listado de tipo ArrayList<Tarea>
     *  2. Eliminar La tarea cuyo código coincide con @codigo del listado
     *  3. Abrir el archivo para reescribirlo
     *  4. Guardar el listado de tareas completo en el fichero
     *
     * @param codigo
     */
    @Override
    public void delete(int codigo) {
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Convierte @register que representa un registro leido del fichero a un objeto de tipo tarea
     *
     * @param register (codigo;usuario;descripcion;fecha de creación;prioridad;vencimiento:realizado)
     * @return Tarea
     */
    private Tarea mapToTarea(String register) {
        throw new RuntimeException("Not yet implemented");
    }

    /**
     * Convierte @Tarea en un String que representa un registro en el fichero de tareas
     *
     * @param tarea
     * @return (codigo;usuario;descripcion;fecha de creación;prioridad;vencimiento:realizado)
     */
    private String mapToRegister(Tarea tarea) {
        throw new RuntimeException("Not yet implemented");
    }
}
