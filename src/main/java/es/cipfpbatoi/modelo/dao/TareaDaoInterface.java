package es.cipfpbatoi.modelo.dao;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.entidades.Tarea;

import java.time.LocalDate;
import java.util.ArrayList;

public interface TareaDaoInterface {

    ArrayList<Tarea> findAll();

    ArrayList<Tarea> findAllWithParams(Boolean isRealizada, LocalDate fecha, String usuario);

    ArrayList<Tarea> findAllWithParams(String usuario);

    Tarea findById(int id);

    Tarea getById(int codigo) throws NotFoundException;

    void delete(int codigo);

    void add(Tarea tarea);

}




