package es.cipfpbatoi.modelo.repositorios;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.dao.FileTareaDAO;
import es.cipfpbatoi.modelo.dao.TareaDaoInterface;
import es.cipfpbatoi.modelo.entidades.Tarea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TareaRepository {

    private TareaDaoInterface fileTareaDAO;

    public TareaRepository(@Autowired FileTareaDAO fileTareaDAO) {
        this.fileTareaDAO = fileTareaDAO;
    }

    /**
     * Obtiene la Tarea con codigo @codTarea. En caso de que no la encuentre devolverá una excepción @NotFoundException
     * @param codigo
     */
    public Tarea get(int codigo) throws NotFoundException {
        return fileTareaDAO.getById(codigo);
    }

    /**
     * Añade la Tarea recibida como argumento a la base de datos en memoria
     * @param tarea
     */
    public void add(Tarea tarea) {
        fileTareaDAO.add(tarea);
    }

    public Tarea find(int codTarea) {
        return fileTareaDAO.findById(codTarea);
    }

    /**
     *  Devuelve el listado de todas las tareas.
     */
    public ArrayList<Tarea> findAll() {
        return fileTareaDAO.findAll();
    }

    /**
     *  Devuelve el listado de todas las tareas cuyo atributo nombre coincide con @user
     */
    public ArrayList<Tarea> findAll(String user) {
        return fileTareaDAO.findAllWithParams(user);
    }

    /**
     * Borra una tarea a partir de su código
     * @param tarea
     */
    public void remove(Tarea tarea) {
        fileTareaDAO.delete(tarea.getCodigo());
    }
}
