package es.cipfpbatoi.modelo.dao;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.entidades.Tarea;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class TestFileTareaDAO {

    @Autowired FileTareaDAO fileTareaDAO;

    @BeforeEach
    public void loadFixtures() {
        try {
            Path file = Path.of(getClass().getResource("/databases/tareas_fixture.txt").getFile());
            Path fileDatabase = Path.of(getClass().getResource("/databases/tareas.txt").getFile());
            Files.copy(file, fileDatabase, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void testGetByIdExistingTask() {
        final int codigoTareaABuscar = 2;
        Assertions.assertDoesNotThrow(()->{
            fileTareaDAO.getById(codigoTareaABuscar);
        }, "No deberia haber lanzado una excepción al buscar una tarea existente codTarea -> " + codigoTareaABuscar);
    }

    @Test
    public void testGetByIdNotExistingTaskThrowsException() {
        final int codigoTareaABuscar = 70;
        Assertions.assertThrows(NotFoundException.class, ()->{
            fileTareaDAO.getById(codigoTareaABuscar);
        }, "Deberia haber lanzado una excepción al buscar una tarea que no existe codTarea -> " + codigoTareaABuscar);
    }

    @Test
    public void testFindByIdExistingTask() {
        final int codigoTareaABuscar = 2;
        //2;Elena;Estudiar programación;2021-11-04 12:00:00;ALTA;2023-04-06 15:00:00;false
        Tarea tarea = fileTareaDAO.findById(codigoTareaABuscar);
        Assertions.assertNotNull(tarea);
        Assertions.assertEquals(tarea.getCodigo(),2);
        Assertions.assertEquals(tarea.getUsuario(),"Elena");
        Assertions.assertEquals(tarea.getDescripcion(),"Estudiar programación");
        Assertions.assertEquals(tarea.getCreadoEn(), LocalDateTime.of(2021,11,4,12,0,0));
        Assertions.assertEquals(tarea.getPrioridad(), Tarea.Priority.ALTA);
        Assertions.assertEquals(tarea.getVencimiento(),LocalDateTime.of(2023,4,6,15,0,0));
        Assertions.assertFalse(tarea.isRealizado());
    }

    @Test
    public void testFindByIdNotExistingTaskReturnNull() {
        final int codigoTareaABuscar = 50;
        Tarea tarea = fileTareaDAO.findById(codigoTareaABuscar);
        Assertions.assertNull(tarea, "Al buscar una tarea que no existe debería retornar null");
    }

    @Test
    public void testFindAllTask() {
        List<Tarea> tareaList = fileTareaDAO.findAll();
        Assertions.assertEquals(tareaList.size(), 14, "Deberia haber leido 14 registros");
    }

    @Test
    public void testFindAllWithParamsIsRealizada() {
        List<Tarea> tareaList = fileTareaDAO.findAllWithParams(true, null, null);
        Assertions.assertEquals(tareaList.size(), 9, "Deberia haber leido solo 9 tareas con realizado = true");
    }

    @Test
    public void testFindAllWithParamsDate() {
        LocalDate localDateParam =  LocalDate.of(2023, 1, 5);
        List<Tarea> tareaList = fileTareaDAO.findAllWithParams(null, localDateParam, null);
        Assertions.assertEquals(tareaList.size(), 1, "Deberia haber leido solo 1 tareas realizadas con fecha " + localDateParam);
    }

    @Test
    public void testFindAllWithParamsUsuario() {
        List<Tarea> tareaList = fileTareaDAO.findAllWithParams(null, null, "Batoi");
        Assertions.assertEquals(3, tareaList.size(), "Deberia haber leido solo 3 tareas realizadas con el usuario Batoi");
    }

    @Test
    public void testFindAllWithParamsIsRealizadaAndDate() {
        List<Tarea> tareaList = fileTareaDAO.findAllWithParams(true ,LocalDate.of(2023, 4, 6), null);
        Assertions.assertEquals(4, tareaList.size(), "Deberia haber leido solo 3 tareas realizadas con el usuario Batoi");
    }

    @Test
    public void testFindAllWithParamsIsRealizadaAndDateAndUsuario() {
        List<Tarea> tareaList = fileTareaDAO.findAllWithParams(true, LocalDate.of(2023, 1, 3), "Juan");
        Assertions.assertEquals(1, tareaList.size(), "Deberia haber leido solo 1 tarea no realizada de fecha 2022-1-6 con el usuario Alex");
    }

    @Test
    public void testAddTask() {
        int codigo = 60;
        String usuario = "test";
        String descripcion = "descripcion Test";
        LocalDateTime creadoEn = LocalDateTime.of(2023,12,12,12,0,0);
        Tarea.Priority priority = Tarea.Priority.MEDIA;
        LocalDateTime vencimiento = creadoEn.plusDays(1);
        boolean realizado = true;
        Tarea tarea = new Tarea(codigo, usuario, descripcion, creadoEn, priority, vencimiento, realizado);
        fileTareaDAO.add(tarea);
        tarea = fileTareaDAO.findById(codigo);
        Assertions.assertNotNull(tarea);
        Assertions.assertEquals(tarea.getCodigo(),codigo);
        Assertions.assertEquals(tarea.getUsuario(),usuario);
        Assertions.assertEquals(tarea.getDescripcion(),descripcion);
        Assertions.assertEquals(tarea.getCreadoEn(), creadoEn);
        Assertions.assertEquals(tarea.getPrioridad(), priority);
        Assertions.assertEquals(tarea.getVencimiento(), vencimiento);
        Assertions.assertEquals(tarea.isRealizado(), realizado);
    }

    @Test
    public void testRemoveTask() {
        final int codigoTareaABuscar = 2;
        fileTareaDAO.delete(codigoTareaABuscar);
        Tarea tarea= fileTareaDAO.findById(codigoTareaABuscar);
        Assertions.assertNull(tarea);
    }

}
